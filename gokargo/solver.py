from gokargo import connected_component as con

# cargos' id and flights (flight_number, scheduled_departure) suggest to be unique
def get_assignments(cargos, flights):
	cargos_by_dest = con.group_cargos(cargos)
	flights_by_dest = con.group_flights(flights)

	for k, v in cargos_by_dest.items():
		cargos_by_dest[k] = sorted(v, reverse=True)
	for k, v in flights_by_dest.items():
		flights_by_dest[k] = sorted(v)

	assignments = [] # should be populated as list of tuples(cargo, flight)
	for dest in cargos_by_dest.keys():
		cargos_to_dest = cargos_by_dest[dest]
		flights_to_dest = flights_by_dest[dest]
		tmp = solve(cargos_to_dest, flights_to_dest)
		assignments.extend(tmp)

	cost = 0
	for k, v in assignments:
		cost += get_cost(k, v)
		
	return cost, assignments

# cargos should be sorted by time descending
# flights should be sorted by time ascending
def solve(cargos_to_dest, flights_to_dest):
	weights = [0] * len(flights_to_dest) # set empty weights
	assignments = []
	assign(0, cargos_to_dest, flights_to_dest, weights, assignments)

	return assignments

def assign(posC, cargos, flights, weights, assignments):
	if posC >= len(cargos): return True # valid assignments

	# try any valid assignments
	for posF in range(len(flights)):
		cargo = cargos[posC]
		flight = flights[posF]
		cur_weight = weights[posF]

		if is_valid_assignment(cargo, flight, cur_weight):
			# try to assign cargo to flight
			weights[posF] += cargo.weight

			if (assign(posC + 1, cargos, flights, weights, assignments)): # assign this
				assignments.append((cargo, flight))
				return True
			else: # revert the assignment
				weights[posF] -= cargo.weight

	return False # there isn't any valid assignment

def is_valid_assignment(cargo, flight, cur_weight):
	if (cur_weight + cargo.weight <= flight.max_weight_capacity and
		flight.scheduled_departure <= cargo.due_date):
		return True
	return False


def get_cost(cargo, flight):
	t = cargo.due_date
	s = flight.scheduled_departure

	delta = t - s
	seconds = delta.total_seconds()

	return seconds * seconds
