from validator_collection import checkers
from datetime import datetime
from .cargo import Cargo
from .flight import Flight
import csv

class InputError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

DATETIME_FORMAT_CODE = "%d-%m-%Y %H:%M:%S"


def validate_cargo_args(id, destination, weight, due_date_string):
    try:
        due_date = datetime.strptime(due_date_string, DATETIME_FORMAT_CODE)
    except Exception:
        due_date = ""
    return checkers.is_string(id) \
        and checkers.is_string(destination) \
        and checkers.is_not_empty(destination) \
        and checkers.is_float(value=weight, minimum=0) \
        and checkers.is_datetime(due_date)


def validate_flight_args(flight_number, destination, scheduled_departure_str, max_weight_capacity):
    try:
        scheduled_departure = datetime.strptime(scheduled_departure_str, DATETIME_FORMAT_CODE)
    except Exception:
        scheduled_departure = ""

    return checkers.is_string(flight_number) \
        and checkers.is_not_empty(flight_number) \
        and checkers.is_string(destination) \
        and checkers.is_not_empty(destination) \
        and checkers.is_datetime(scheduled_departure) \
        and checkers.is_float(value=max_weight_capacity, minimum=0)

def parse_csv(csv_cargo, csv_flight):
    read_cargo = csv.reader(csv_cargo, delimiter=',')
    read_flight = csv.reader(csv_flight, delimiter=',')
    cargos = []
    flights = []
    for i, row in enumerate(read_cargo):
        if len(row) == 4 and validate_cargo_args(row[0], row[1], row[2], row[3]):
            id = row[0]
            dest = row[1]
            weight = float(row[2])
            due_date = datetime.strptime(row[3], DATETIME_FORMAT_CODE)
            cargo = Cargo(id, dest, weight, due_date)
            cargos.append(cargo)
        elif i > 0: # Ignore error for the first line
            raise InputError("Data kargo yang diberikan tidak sesuai format!")
    
    for i, row in enumerate(read_flight):
        if len(row) == 4 and validate_flight_args(row[0], row[1], row[2], row[3]):
            flight_num = row[0]
            dest = row[1]
            date_depart = datetime.strptime(row[2], DATETIME_FORMAT_CODE)
            max_weight = float(row[3])
            flight = Flight(flight_num, dest, date_depart, max_weight)
            flights.append(flight)
        elif i > 0: # Ignore error for the first line
            raise InputError("Data penerbangan yang diberikan tidak sesuai format!")
    return cargos, flights

def read_file(f):
    try:
        return "".join([i.decode('utf-8') for i in f.chunks()])
    except UnicodeDecodeError as e:
        raise InputError("Berkas yang diunggah harus merupakan berkas teks!")

def solve_result_to_csv(solve_result):
    res = [(c.id, f.flight_number, f.scheduled_departure.strftime("%d-%m-%Y %H:%M:%S"))\
        for c, f in solve_result]
    res_str = "\n".join([",".join([i for i in lines]) for lines in res])
    return res_str

def convert_to_json(cargos, flights, result, cost):
    # Group flights by its flight number
    flight_dict = {}
    for f in flights:
        flightset = flight_dict.setdefault(f.flight_number, set())
        flightset.add(f.scheduled_departure)
    
    # For every grouped flights,
    # sort by date and number them consecutively
    for f in flight_dict:
        flight_dict[f] = dict([(j, i) for i, j in\
            enumerate(sorted(flight_dict[f]), 1)])
    
    # For now, every flight will be identified by
    # its flight name and order number
    flight_detail = {}
    for f in flights:
        f_num = f.flight_number
        id_data = f.flight_number + "-" +\
            str(flight_dict[f_num][f.scheduled_departure])
        flight_detail[id_data] = f.toJSON()
        
    cargo_detail = {}
    for c in cargos:
        cargo_detail[c.id] = c.toJSON()
    
    solveRes = {}
    for c, f in result:
        f_num = f.flight_number
        id_data = f.flight_number + "-" +\
            str(flight_dict[f_num][f.scheduled_departure])
        solveRes[c.id] = id_data
    
    paired_cargos = [c.id for c, f in result]
    unpaired_cargos = [c.id for c in cargos if c.id not in paired_cargos]
    flights = [f for f in flight_detail.keys()]
    
    dataSolve = {'paired_cargos' : paired_cargos,
        'unpaired_cargos' : unpaired_cargos,
        'flight_detail' : flight_detail,
        'cargo_detail' : cargo_detail,
        'flights' : flights,
        'paired_cargos' : paired_cargos,
        'unpaired_cargos' : unpaired_cargos,
        'solve_result' : solveRes,
        'cost': cost,
        'csv' : solve_result_to_csv(result)
    }
    
    return dataSolve