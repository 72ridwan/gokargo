class Cargo:
	def __init__(self, id, destination, weight, due_date):
		self.id = id
		self.destination = destination
		self.weight = weight
		self.due_date = due_date

	def __lt__(self, other):
		if self.due_date == other.due_date:
			return self.id < other.id
		return self.due_date < other.due_date
	
	def toJSON(self):
		dct = {}
		dct['id'] = str(self.id)
		dct['dest'] = str(self.destination)
		dct['due_date'] = self.due_date.strftime("%d-%m-%Y %H:%M:%S")
		dct['weight'] = str(self.weight)
		return dct