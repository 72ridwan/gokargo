class Flight:
	def __init__(self, flight_number, destination, scheduled_departure, max_weight_capacity):
		self.flight_number = flight_number
		self.destination = destination
		self.scheduled_departure = scheduled_departure
		self.max_weight_capacity = max_weight_capacity

	def __lt__(self, other):
		if self.scheduled_departure == other.scheduled_departure:
			return self.flight_number < other.flight_number
		return self.scheduled_departure < other.scheduled_departure
	
	def toJSON(self):
		dct = {}
		dct['flight_num'] = str(self.flight_number)
		dct['dest'] = str(self.destination)
		dct['depart_time'] = self.scheduled_departure.strftime("%d-%m-%Y %H:%M:%S")
		dct['max_weight'] = str(self.max_weight_capacity)
		return dct