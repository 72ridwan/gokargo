def group_cargos(cargos):
    cargos_by_dest = {}
    for cargo in cargos:
        cargos_by_dest.setdefault(cargo.destination, []).append(cargo)
    return cargos_by_dest

def group_flights(flights):
    flights_by_dest = {}
    for flight in flights:
        flights_by_dest.setdefault(flight.destination, []).append(flight)
    return flights_by_dest
