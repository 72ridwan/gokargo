var make_tooltip_cargo = function(name, weight, dest, deadline) {
  return '<table><tbody><tr><th>Nama</th><td>' + name +
    '</td></tr><tr><th>Berat</th><td>' + weight +
    ' kg</td></tr><tr><th>Destinasi</th><td>' + dest +
    '</td></tr><tr><th>Waktu <i>deadline</i> </th><td>' + deadline +
    '</td></tr></tbody></table>';
};
var make_tooltip_flight = function(fnum, dest, depart, capacity) {
  return '<table><tbody><tr><th>Nama</th><td>' + fnum +
    '</td></tr><tr><th>Destinasi</th><td>' + dest +
    '</td></tr><tr><th>Waktu keberangkatan</th><td>' + depart +
    '</td></tr><tr><th>Kapasitas </th><td>' + capacity +
    ' kg</td></tr></tbody></table>';
};
var initialize_network = function(container, container_l) {
  var color_cargo = {
    'background': '#EFD259',
    'border': '#84560D',
    'hover': {
      'background': '#FFE885',
      'border': '#84560D'
    },
    'highlight': {
      'background': '#FFE885',
      'border': '#A56304'
    },
  }
  var color_cargo_unpaired = {
    'background': '#FF6E7C',
    'border': '#862C35',
    'hover': {
      'background': '#FFB39F',
      'border': '#84560D'
    },
    'highlight': {
      'background': '#FFB39F',
      'border': '#B21919'
    },
  }
  var color_flight = {
    'background': '#75B3EA',
    'border': '#2E567A',
    'hover': {
      'background': '#A6E4FF',
      'border': '#2E567A'
    },
    'highlight': {
      'background': '#A6E4FF',
      'border': '#19417C'
    },
  }
  var options = {
    'nodes': {
      'borderWidth': 2,
      'borderWidthSelected': 4,
      'font': {
        'face': 'Open Sans'
      }
    },
    'groups': {
      'cargos': {
        'color': color_cargo,
        'shape': 'square'
      },
      'flights': {
        'color': color_flight,
        'shape': 'triangle'
      },
      'cargos_unpaired': {
        'color': color_cargo_unpaired,
        'shape': 'square'
      },
    },
    'physics': {
      'enabled': false
    },
    'interaction': {
      'hover': true,
      'dragNodes': false
    },    
    'layout': {
      'hierarchical': {
        'enabled': true,
        'treeSpacing': 95,
        'levelSeparation': 100,
        'nodeSpacing': 95,
        'edgeMinimization': false,
        'direction': 'RL',
        'sortMethod': 'hubsize'
      },
    },
  };
  var options_l = {
    'nodes': {
      'borderWidth': 2,
      'borderWidthSelected': 4,
      'font': {
        'face': 'Open Sans'
      }
    },
    'groups': {
      'cargos': {
        'color': color_cargo,
        'shape': 'square'
      },
      'flights': {
        'color': color_flight,
        'shape': 'triangle'
      },
      'cargos_unpaired': {
        'color': color_cargo_unpaired,
        'shape': 'square'
      },
    },
    'physics': {
      'enabled': false
    },
    'interaction': {
      'hover': false,
      'selectable': false,
      'dragView': false,
      'dragNodes': false,
      'zoomView': false
    },
    'layout': {
      'hierarchical': {
        'enabled': true,
        'treeSpacing': 80,
        'edgeMinimization': false,
        'direction': 'UD'
      },
    },
  };
  var visdata_l = {
    'nodes': new vis.DataSet([{
        id: 1,
        label: 'Kargo\ntanpa\npasangan',
        group: 'cargos_unpaired'
      },
      {
        id: 2,
        label: 'Kargo',
        group: 'cargos'
      },
      {
        id: 3,
        label: 'Penerbangan',
        group: 'flights'
      }
    ])
  };
  var network = new vis.Network(container.get(0), {}, options);
  var netCanvas = container.get(0).getElementsByTagName("canvas")[0];
  var changeCursor = function(newCursorStyle) {
    netCanvas.style.cursor = newCursorStyle;
  }
  container.hover(function() {
    changeCursor('grab');
  });
  var network_l = new vis.Network(container_l.get(0), visdata_l,
    options_l);
  return {
    'network': network,
    'network_l': network_l
  };
};
var display_network = function(network, data) {
  var visnodes = [];
  var visedges = [];
  var dict_cargos = {};
  var dict_flights = {};
  // Set all paired and unpaired cargos, as well as the flights,
  // as Vis nodes.
  for (var i in data.paired_cargos) {
    var c = data.paired_cargos[i];
    var cdet = data.cargo_detail[c];
    var id = visnodes.length + 1;
    dict_cargos[c] = id;
    visnodes.push({
      'id': id,
      'label': c,
      group: 'cargos',
      title: make_tooltip_cargo(cdet.id, cdet.weight, cdet.dest,
        cdet.due_date)
    });
  }
  for (var i in data.unpaired_cargos) {
    var c = data.unpaired_cargos[i];
    var cdet = data.cargo_detail[c];
    var id = visnodes.length + 1;
    dict_cargos[c] = id;
    visnodes.push({
      'id': id,
      'label': c,
      group: 'cargos_unpaired',
      title: make_tooltip_cargo(cdet.id, cdet.weight, cdet.dest,
        cdet.due_date)
    });
  }
  for (var i in data.flights) {
    var f = data.flights[i];
    var fdet = data.flight_detail[f];
    var id = visnodes.length + 1;
    dict_flights[f] = id;
    visnodes.push({
      'id': id,
      'label': f,
      group: 'flights',
      title: make_tooltip_flight(fdet.flight_num, fdet.dest,
        fdet.depart_time, fdet.max_weight)
    });
  }
  // Connect all paired cargos.
  for (c in data.solve_result) {
    var p = data.solve_result[c];
    visedges.push({
      'from': dict_cargos[c],
      'to': dict_flights[p]
    });
  }
  var visdata = {
    'nodes': new vis.DataSet(visnodes),
    'edges': new vis.DataSet(visedges)
  };
  network.setData(visdata);
}