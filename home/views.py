from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.urls import reverse
from django.http import JsonResponse
from gokargo.classes.utils import *
from gokargo import solver
from io import StringIO

dataSolve = {} # Solve result

def index (request):
    return render(request, "home.html", {})

def submitted_data (request):
    if request.method == "POST" and 'file-cargo' in request.FILES\
        and 'file-flight' in request.FILES:
        try:
            cargo_string = StringIO(read_file(request.FILES.get('file-cargo')))
            flight_string = StringIO(read_file(request.FILES.get('file-flight')))
            cargos, flights = parse_csv(cargo_string, flight_string)
            cost, result = solver.get_assignments(cargos, flights)
            dataSolve = convert_to_json(cargos, flights, result, cost)
            return JsonResponse(dataSolve)
        except InputError as e:
            return HttpResponse(status=400, content=e.value)
    return HttpResponse(status=400, content="Bad Request.")