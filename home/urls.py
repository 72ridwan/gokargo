from django.conf.urls import url
from .views import index, submitted_data

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^submit-data$', submitted_data, name='submit_data'),
]