from django.test import TestCase
from gokargo import solver
from datetime import datetime, timedelta
from gokargo.classes.cargo import *
from gokargo.classes.flight import *
import unittest

time_now = datetime.now()

# Create your tests here.
class Test(TestCase, unittest.TestCase):
	def test_get_cost(self):
		cargos = [Cargo('C1', 'Depok', 3, time_now + timedelta(hours=1))]
		cargos.append(Cargo('C2', 'Depok', 5, time_now + timedelta(hours=10)))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=30), 5))

		self.assertAlmostEqual(solver.get_cost(cargos[0], flights[0]), 12960000)
		self.assertAlmostEqual(solver.get_cost(cargos[0], flights[1]), 3240000)
		self.assertAlmostEqual(solver.get_cost(cargos[1], flights[0]), 1296000000)
		self.assertAlmostEqual(solver.get_cost(cargos[1], flights[1]), 1169640000)

	def test_is_valid_assignment(self):
		cargos = [Cargo('C1', 'Depok', 3, time_now)]
		cargos.append(Cargo('C2', 'Depok', 5, time_now + timedelta(seconds=3)))

		flights = [Flight('F1', 'Depok', time_now - timedelta(hours=1), 3)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(seconds=3), 5))
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=3), 10))

		# test weight
		self.assertTrue(solver.is_valid_assignment(cargos[0], flights[0], 0))
		self.assertFalse(solver.is_valid_assignment(cargos[0], flights[0], 1))
		self.assertFalse(solver.is_valid_assignment(cargos[1], flights[0], 0))

		# test time
		self.assertFalse(solver.is_valid_assignment(cargos[0], flights[1], 0))
		self.assertTrue(solver.is_valid_assignment(cargos[1], flights[1], 0))
		self.assertFalse(solver.is_valid_assignment(cargos[1], flights[2], 0))

	# standard case to check optimal assignment
	def test_solve_0(self):
		cargos = [Cargo('C2', 'Depok', 5, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C1', 'Depok', 3, time_now + timedelta(hours=1)))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=30), 5))

		result = solver.solve(cargos, flights)

		self.assertEqual(result[0][0].id, 'C1')
		self.assertEqual(result[0][1].flight_number, 'F2')

		self.assertEqual(result[1][0].id, 'C2')
		self.assertEqual(result[1][1].flight_number, 'F1')

	# the most optimal assignment is violating the time constraint
	def test_solve_1(self):
		cargos = [Cargo('C2', 'Depok', 5, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C1', 'Depok', 3, time_now + timedelta(hours=1)))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=61), 5))

		result = solver.solve(cargos, flights)

		self.assertEqual(result[0][0].id, 'C1')
		self.assertEqual(result[0][1].flight_number, 'F1')

		self.assertEqual(result[1][0].id, 'C2')
		self.assertEqual(result[1][1].flight_number, 'F2')

	# no flight valid for 'C2' cargo
	def test_solve_2(self):
		cargos = [Cargo('C2', 'Depok', 5, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C1', 'Depok', 3, time_now + timedelta(hours=1)))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(hours=10, seconds=1), 5))

		result = solver.solve(cargos, flights)
		self.assertEqual(len(result), 0) # no valid assignment

	# case with more than one cargo in a flight
	def test_solve_3(self):
		cargos = [Cargo('C3', 'Depok', 5, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C2', 'Depok', 3, time_now + timedelta(hours=1)))
		cargos.append(Cargo('C1', 'Depok', 2, time_now + timedelta(minutes=45)))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=30), 5))

		result = solver.solve(cargos, flights)

		self.assertEqual(result[2][0].id, 'C3')
		self.assertEqual(result[2][1].flight_number, 'F1')

		self.assertEqual(result[1][0].id, 'C2')
		self.assertEqual(result[1][1].flight_number, 'F2')

		self.assertEqual(result[0][0].id, 'C1')
		self.assertEqual(result[0][1].flight_number, 'F2')

	# case with more than one cargo in a flight but violates max weight capacity of the flight
	def test_solve_4(self):
		cargos = [Cargo('C3', 'Depok', 5, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C2', 'Depok', 3, time_now + timedelta(hours=1)))
		cargos.append(Cargo('C1', 'Depok', 3, time_now + timedelta(minutes=45)))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=30), 5))

		result = solver.solve(cargos, flights)

		self.assertEqual(len(result), 0)

	# case with more than one cargo in a flight but violates the time constraint
	def test_solve_5(self):
		cargos = [Cargo('C3', 'Depok', 1, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C2', 'Depok', 4, time_now + timedelta(minutes=25)))
		cargos.append(Cargo('C1', 'Depok', 2, time_now))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=30), 5))

		result = solver.solve(cargos, flights)

		self.assertEqual(len(result), 0)

	# case with more than one cargo in a flight but not the most optimal assignment (because time constraint violation)
	def test_solve_6(self):
		cargos = [Cargo('C3', 'Depok', 1, time_now + timedelta(hours=10))]
		cargos.append(Cargo('C2', 'Depok', 3, time_now + timedelta(hours=1)))
		cargos.append(Cargo('C1', 'Depok', 2, time_now))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F2', 'Depok', time_now + timedelta(minutes=30), 3))

		result = solver.solve(cargos, flights)

		self.assertEqual(result[2][0].id, 'C3')
		self.assertEqual(result[2][1].flight_number, 'F1')

		self.assertEqual(result[1][0].id, 'C2')
		self.assertEqual(result[1][1].flight_number, 'F2')

		self.assertEqual(result[0][0].id, 'C1')
		self.assertEqual(result[0][1].flight_number, 'F1')
	# mix case with more than one destination
	def test_get_assignments_0(self):
		cargos = [Cargo('C2', 'Depok', 3, time_now + timedelta(hours=1))]
		cargos.append(Cargo('C3', 'Depok', 1, time_now + timedelta(hours=10)))
		cargos.append(Cargo('C5', 'Depok', 2, time_now))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F3', 'Depok', time_now + timedelta(minutes=30), 3))

		cargos.append(Cargo('C4', 'Makassar', 5, time_now + timedelta(hours=1)))
		cargos.append(Cargo('C1', 'Makassar', 5, time_now + timedelta(hours=10)))

		flights.append(Flight('F2', 'Makassar', time_now + timedelta(seconds=1), 5))
		flights.append(Flight('F4', 'Makassar', time_now, 5))

		cost, assignments = solver.get_assignments(cargos, flights)

		self.assertAlmostEqual(cost, 2608192801)

		for c, f in assignments:
			if c.id == 'C1':
				self.assertEqual(f.flight_number, 'F4')
			elif c.id == 'C2':
				self.assertEqual(f.flight_number, 'F3')
			elif c.id == 'C3':
				self.assertEqual(f.flight_number, 'F1')
			elif c.id == 'C4':
				self.assertEqual(f.flight_number, 'F2')
			elif c.id == 'C5':
				self.assertEqual(f.flight_number, 'F1')

	# mix case with more than one destination but one dest is not valid
	def test_get_assignments_1(self):
		cargos = [Cargo('C2', 'Depok', 3, time_now + timedelta(hours=1))]
		cargos.append(Cargo('C3', 'Depok', 1, time_now + timedelta(hours=10)))
		cargos.append(Cargo('C5', 'Depok', 2, time_now))

		flights = [Flight('F1', 'Depok', time_now, 5)]
		flights.append(Flight('F3', 'Depok', time_now + timedelta(minutes=30), 3))

		cargos.append(Cargo('C4', 'Makassar', 5, time_now))
		cargos.append(Cargo('C1', 'Makassar', 5, time_now))

		flights.append(Flight('F2', 'Makassar', time_now + timedelta(seconds=1), 5))
		flights.append(Flight('F4', 'Makassar', time_now, 5))

		cost, assignments = solver.get_assignments(cargos, flights)

		self.assertAlmostEqual(cost, 1299240000)
		self.assertEqual(len(assignments), 3)
